using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Composition;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CodeFixes;
using Microsoft.CodeAnalysis.CodeActions;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Formatting;
using Microsoft.CodeAnalysis.Rename;
using Microsoft.CodeAnalysis.Text;

namespace TA.Ascom.DriverBestPracticesAnalyzer
    {
    [ExportCodeFixProvider(LanguageNames.CSharp, Name = nameof(DriverAttributesCodeFixProvider)), Shared]
    public class DriverAttributesCodeFixProvider : CodeFixProvider
        {
        private const string titleAddProgId = "Add COM Prog ID attribute to ASCOM driver class";
        private const string titleMakePublic = "Make ASCOM driver class public";
        private const string titleMakeComVisible = "Make ASCOM driver class COM visible";
        private const string titleAddClassId = "Add COM Class ID attribute to ASCOM driver class";
        private const string titleMakeLateBound = "Make ASCOM driver class use late binding";
        private const string titleAddServedClassName = "Add ServedClassName attribute to ASCOM driver class";
        private const string titleInheritRefCountedObject = "Inherit from ReferenceCountedObjectBase";

        public sealed override ImmutableArray<string> FixableDiagnosticIds
            {
            get { return ImmutableArray.Create(AscomDriverBestPracticesAnalyzer.MissingProgIdDiagnosticId); }
            }

        public sealed override FixAllProvider GetFixAllProvider()
            {
            // See https://github.com/dotnet/roslyn/blob/master/docs/analyzers/FixAllProvider.md for more information on Fix All Providers
            return WellKnownFixAllProviders.BatchFixer;
            }

        public sealed override async Task RegisterCodeFixesAsync(CodeFixContext context)
            {
            var root = await context.Document.GetSyntaxRootAsync().ContinueOnAnyThread();
            var model = await context.Document.GetSemanticModelAsync().ContinueOnAnyThread();

            foreach (var diagnostic in context.Diagnostics)
                {
                var diagnosticSpan = diagnostic.Location.SourceSpan;
                var classDefinition = root.FindToken(diagnosticSpan.Start)
                    .Parent
                    .AncestorsAndSelf()
                    .OfType<ClassDeclarationSyntax>()
                    .First();
                switch (diagnostic.Id)
                    {
                    case AscomDriverBestPracticesAnalyzer.MissingProgIdDiagnosticId:
                        context.RegisterCodeFix(CodeAction.Create(titleAddProgId,
                                c => AddProgIdToClass(context.Document, classDefinition,
                                    c),
                                titleAddProgId),
                            diagnostic);
                        break;
                    default:
                        return;
                    }
                }
            }

        private async Task<Document> AddProgIdToClass(Document document, ClassDeclarationSyntax classDeclaration,
            CancellationToken cancel)
            {
            // ToDo: make a generic method for adding an attribute
            var root = await document.GetSyntaxRootAsync(cancel);
            var attributes = classDeclaration.AttributeLists.Add(
                SyntaxFactory.AttributeList(SyntaxFactory.SingletonSeparatedList<AttributeSyntax>(
                        SyntaxFactory.Attribute(SyntaxFactory.IdentifierName("ProgId"))
                            .WithArgumentList(
                                SyntaxFactory.AttributeArgumentList(
                                    SyntaxFactory.SingletonSeparatedList<AttributeArgumentSyntax>(
                                        SyntaxFactory.AttributeArgument(
                                            SyntaxFactory.LiteralExpression(
                                                SyntaxKind.StringLiteralExpression,
                                                SyntaxFactory.Literal("ASCOM.MyDeviceName.Dome"))))))))
                    .WithAdditionalAnnotations(Formatter.Annotation)
                    .NormalizeWhitespace());
            return document.WithSyntaxRoot(
                root.ReplaceNode(
                    classDeclaration,
                    classDeclaration.WithAttributeLists(attributes)));
            }
        }
    }
