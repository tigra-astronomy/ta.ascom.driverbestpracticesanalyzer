using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Threading;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Diagnostics;

namespace TA.Ascom.DriverBestPracticesAnalyzer
    {
    [DiagnosticAnalyzer(LanguageNames.CSharp)]
    public class AscomDriverBestPracticesAnalyzer : DiagnosticAnalyzer
        {
        public const string MissingProgIdDiagnosticId = "ASCOM001";
        public const string ShouldBePublicDiagnosticId = "ASCOM002";
        public const string ShouldBeComVisibleDiagnosticId = "ASCOM003";
        public const string ShouldHaveClsIdiagnosticId = "ASCOM004";
        public const string ShouldBeLateBoundDiagnosticId = "ASCOM005";
        public const string ShouldHaveServedClassNameDiagnosticId = "ASCOM006";
        public const string ShouldInheritRefCountedObjectDiagnosticId = "ASCOM007";

        private static readonly LocalizableString Ascom001Title = "ASCOM Drivers should have a ProgId attribute";
        private static readonly LocalizableString Ascom001MessageFormat =
            "ASCOM Driver Class '{0}' should have a [ProgId()] attribute";
        private static readonly LocalizableString Ascom001Description = "ASCOM Driver is missing ProgId attribute";
        private const string Category = "ASCOM Driver Best Practices";

        private static DiagnosticDescriptor ProgIdRule = new DiagnosticDescriptor(MissingProgIdDiagnosticId, Ascom001Title, Ascom001MessageFormat, Category, DiagnosticSeverity.Error, isEnabledByDefault: true, description: Ascom001Description);
        private static readonly IList<string> AscomInterfaces = new List<string> {"ICameraV2", "IDomeV2","IFilterWheelV2","IFocuserV3","IObservingConditions","IRotatorV2","ISafetyMonitor","ISwitchV2", "ITelescopeV3", "IVideo"};

        public override ImmutableArray<DiagnosticDescriptor> SupportedDiagnostics { get { return ImmutableArray.Create(ProgIdRule); } }

        public override void Initialize(AnalysisContext context)
            {
            // TODO: Consider registering other actions that act on syntax instead of or in addition to symbols
            // See https://github.com/dotnet/roslyn/blob/master/docs/analyzers/Analyzer%20Actions%20Semantics.md for more information
            //context.RegisterSymbolAction(AnalyzeSymbol, SymbolKind.NamedType);
            context.RegisterSyntaxNodeAction(AnalyzeAscomDriverClass, SyntaxKind.ClassDeclaration);
            }

        private void AnalyzeAscomDriverClass(SyntaxNodeAnalysisContext context)
            {
            var model = context.SemanticModel;
            var classDeclaration = (ClassDeclarationSyntax) context.Node;
            var baseList = classDeclaration?.BaseList;
            if (baseList == null) return;
            var simpleBaseTypes = baseList.Types.Where(p => p.IsKind(SyntaxKind.SimpleBaseType))
                .Select(p => (SimpleBaseTypeSyntax) p);
            var baseTypeNames = simpleBaseTypes.Select(p => p.Type.ToString()).ToList();
            if (!baseTypeNames.Any()) return;
            var ascomInterface = baseTypeNames.FirstOrDefault(interfaceName => AscomInterfaces.Contains(interfaceName));
            if (string.IsNullOrEmpty(ascomInterface)) return;
            // We found a class that implements an ASCOM interface. Get the class name.
            INamedTypeSymbol classNameSymbol = context.SemanticModel.GetDeclaredSymbol(classDeclaration);
            //var classSymbol = context.SemanticModel.GetSymbolInfo(classDeclaration).Symbol as INamedTypeSymbol;
            VerifyHasProgIdAttribute(context, classDeclaration, classNameSymbol, ascomInterface);
            }

        /// <summary>
        /// Verifies whether a class has a [ProgId] attribute.
        /// If none present, reports an error diagnostic.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="classDeclaration">The class declaration.</param>
        /// <param name="classNameSymbol">The class symbol.</param>
        /// <param name="ascomInterface">The ASCOM interface that is being implemented.</param>
        private void VerifyHasProgIdAttribute(SyntaxNodeAnalysisContext context,
            ClassDeclarationSyntax classDeclaration, INamedTypeSymbol classNameSymbol,
            string ascomInterface)
            {
            var attributeListSyntax = classDeclaration.AttributeLists.SelectMany(al => al.Attributes);
            if (attributeListSyntax.Any(a => a.Name.ToString() == "ProgId")) return;
            var diagnostic = Diagnostic.Create(ProgIdRule, classNameSymbol.Locations[0], classNameSymbol.Name);
            context.ReportDiagnostic(diagnostic);
            }

        private static void AnalyzeSymbol(SymbolAnalysisContext context)
            {
            // TODO: Replace the following code with your own analysis, generating Diagnostic objects for any issues you find
            var namedTypeSymbol = (INamedTypeSymbol)context.Symbol;

            // Find just those named type symbols with names containing lowercase letters.
            if (namedTypeSymbol.Name.ToCharArray().Any(char.IsLower))
                {
                // For all such symbols, produce a diagnostic.
                var diagnostic = Diagnostic.Create(ProgIdRule, namedTypeSymbol.Locations[0], namedTypeSymbol.Name);

                context.ReportDiagnostic(diagnostic);
                }
            }
        }
    }
