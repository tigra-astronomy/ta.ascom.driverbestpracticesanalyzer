﻿using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace TA.Ascom.DriverBestPracticesAnalyzer
    {
    public static class TaskExtensions
        {
        public static TResult WaitForResult<TResult>(this Task<TResult> asyncOperation)
            {
            asyncOperation.Wait();
            return asyncOperation.Result;
            }

        /// <summary>
        /// Configures a task continuation to run on the same thread as the task. That is, the
        /// current synchronization context is captured and the task continuation is posted to that
        /// context.
        /// </summary>
        public static ConfiguredTaskAwaitable<T> ContinueOnSameThread<T>(this Task<T> task)
            {
            return task.ConfigureAwait(true);
            }

        /// <summary>
        /// Configures a task continuation to run on any available thread including the current one.
        /// </summary>
        public static ConfiguredTaskAwaitable<T> ContinueOnAnyThread<T>(this Task<T> task)
            {
            return task.ConfigureAwait(false);
            }
        }
    }