﻿# Requirements for ASCOM Driver analyzers

In the following rules, a "driver" is deemed to be a class implementing one of the ASCOM device interfaces.
- Driver should have a ProgID attribute
	- Fix: add a [ProgId] attribute with ProgID="ASCOM.{class-name}.{device-type}"
- Driver should have Guid attribute
	- Fix: add a [Guid()] attribute with a newly generated GUID
- Driver class should be public (fix: make public)
- Driver should inherit ReferenceCountedObject (fix: inherit ReferenceCountedObject as the first inheritor)
- Driver should be late-bound only (must have [ClassInterface(ClassInterfaceType.None)] attribute)
- Driver should have [ServedClassName("Chooser-name")] attribute
- Driver should have only parameterless constructors (no fix, mark as error)
